// Bài 1: Quản lý sinh viên
/**
 * Mô hình 3 khối
 * input: Tạo các nút cần nhập: 
 *    - nhập điểm chuẩn
 *    - Khu vực
 *    - Đối tượng
 *    - Điểm 3 môn
 * 
 * xử lí: Bằng cách so sánh điểm tổng kết với điểm chuẩn
 *    - điểm tổng kết = điểm 3 môn + điểm đối tượng ưu tiên và điểm khu vực ưu tiên
 * 
 * output: trả về 2 trường hợp 
 *    - Số điểm tổng kết bằng hoặc lớn hơn điểm chuẩn sẽ trả về : Bạn đã đậu + số điểm tổng kết
 *    - Số điểm tổng kết thấp hơn điểm chuẩn sẽ trả về : Bạn đã rớt + số điểm tổng kết
 */
document.querySelector('.KETQUA').addEventListener('click',function() {   
   var diemChuan = document.getElementById('txt-diem-chuan').value * 1;
   var khuVucOptionValue = document.getElementById('txt-khu-vuc').value*1;
   var doiTuongOptionValue = document.getElementById('txt-doi-tuong').value*1;
   var diemMon1 = document.getElementById('txt-diemmon-1').value * 1;
   var diemMon2 = document.getElementById('txt-diemmon-2').value * 1;
   var diemMon3 = document.getElementById('txt-diemmon-3').value * 1;

 console.log({khuVucOptionValue,doiTuongOptionValue,diemChuan,diemMon1,diemMon2,diemMon3})
 var diemTongKet = (diemMon1 + diemMon2 + diemMon3) + (khuVucOptionValue + doiTuongOptionValue)
 console.log(diemTongKet)

   if (diemTongKet >= diemChuan) {
      document.querySelector('.duDoan').innerHTML = `Bạn đã đậu , Tổng điểm:${diemTongKet}`
   } else {
      document.querySelector('.duDoan').innerHTML = `Bạn đã rớt , Tổng điểm:${diemTongKet}`
   }
})


// Bài 2: Tính tiền điện
/**
 * Input: Tạo các nút nhập:
 *    - nút nhập tên
 *    - Nút nhập số Kw
 * 
 * Xử lí:
 *    + giá tiền 50Kw đầu = 500 (Bậc 1)
 *    + Giá tiền 51Kw Tới 100Kw = 650 (Bậc 2)
 *    + Giá tiền 101Kw Tới 200Kw = 850 (Bậc 3)
 *    + Giá tiền từ 201Kw Tới 350Kw = 1100 (Bậc 4)
 *    + Giá tiền từ 350Kw trở đi = 1300 (Bậc 5 )
 * 
 * Output: Trả ra số tiền tương ứng khi sang các mức khác nhau
 */

 var Giatien50KwDautien = 500;//Bậc 1
 var Giatien50Kwtoi100Kw = 650; //Bậc 2
 var Giatien100Kwtoi200Kw = 850;// Bậc 3
 var Giatien200Kwtoi350Kw = 1100;// Bậc 4
 var Giatien350KwTroDi = 1300;// Bậc 5

document.querySelector('.Tinh-tien-dien').addEventListener('click',function(){
   var NameValue = document.getElementById('txt-ten').value;
   var soKwValue = document.getElementById('txt-Kw').value*1;

   var SotienPhaiTra

   if(soKwValue <= 50) {
      SotienPhaiTra = soKwValue * Giatien50KwDautien;
   } 
   else if (soKwValue <= 100) {
      SotienPhaiTra = 50 * Giatien50KwDautien + ((soKwValue - 50)*Giatien50Kwtoi100Kw)
   }
   else if (soKwValue <= 200) {
      SotienPhaiTra = 50 * Giatien50KwDautien + (50 * Giatien50Kwtoi100Kw) + ((soKwValue - 100) * Giatien100Kwtoi200Kw)
   }
   else if (soKwValue <= 350) {
      SotienPhaiTra = 50 * Giatien50KwDautien + ( 50 * Giatien50Kwtoi100Kw) + (100 * Giatien100Kwtoi200Kw) + ((soKwValue - 200) * Giatien200Kwtoi350Kw)
   } else {
      SotienPhaiTra = 50 * Giatien50KwDautien+(50 * Giatien50Kwtoi100Kw) + (100 * Giatien100Kwtoi200Kw) + (150 * Giatien200Kwtoi350Kw) + ((soKwValue - 350) * Giatien350KwTroDi)
   }
   console.log(NameValue, SotienPhaiTra)
   document.querySelector('.TinhTienDien').innerHTML = `<div>Tên: ${NameValue} - Tiền điện: ${SotienPhaiTra}d/Kw </div>`
})